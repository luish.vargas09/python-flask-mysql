# set de funciones
import functools
from flask import (
    Blueprint, flash, g, render_template, request, url_for, session, redirect
)
# módulo de seguridad para verificar y crear contraseñas encriptadas
from werkzeug.security import check_password_hash, generate_password_hash
# importamos la bd y su funcion para poder interactuar con ella
from .db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        
        db, c = get_db()
        error = None

        # Con el cursor ejecutamos una consulta
        c.execute(
                'SELECT id FROM user WHERE username = %s', (username, )
            )
        # Verificamos que se hayan ingresado los datos
        if not username:
            error = 'Username es requerido'
        if not password:
            error = 'Password es requerido'
        elif c.fetchone() is not None:
            error = 'Usuario {} se encuentra registrado.'.format(username)
        
        # Sino hay errores insertamos los datos (encryptamos el password)
        if error is None:
            c.execute(
                'INSERT INTO user (username, password) VALUES (%s, %s)',
                (username, generate_password_hash(password))
            )
            # Comprometemos la BD para que se insepte con éxito
            db.commit()
            # Redireccionamos al Login para que acceda
            return redirect(url_for('auth.login'))
        # Si hay error mandamos un mensaje
        flash(error)
    # Sino hay petición POST salimos y renderizamos la misma página
    return render_template('auth/register.html')

@bp.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        # Esto es lo que el usuario ingresa en los inputs 
        username = request.form['username']
        password = request.form['password']

        # Creamos un cursor para las consultas y un db para el commit
        db, c = get_db()
        error = None
        # Realizamos nuestra búsqueda
        c.execute(
            'SELECT * FROM user WHERE username = %s',
            (username,)
        )
        # Sacamos los datos del usuario de la consulta 
        user = c.fetchone()
        # flash(user)

        # Si el dato no coincide con la búsqueda mandamos error
        if user is None:
            error = 'Usuario y/o contraseña inválda'
        # Checamos si la contraseña, que tenemos en la BD de tal 
        # usuario, coincide con la que el envía por POST
        elif not check_password_hash(user['password'], password):
            error = 'Usuario y/o contraseña inválda'
        
        # Sino existe ningún error 
        if error is None:
            # limpiamos la sesión 
            session.clear()
            # Colocamos dentro de la sesión el id del usuario
            session['user_id'] = user['id']
            # Terminamos redireccionando a inicio
            return redirect(url_for('todo.index'))
        
        # Si existe un error lo muestra en mensaje
        flash(error)
    
    # Si el método NO es POST retornamos la plantilla del login
    return render_template('auth/login.html')

@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        db, c = get_db()
        c.execute(
            'SELECT  * FROM user WHERE id = %s', (user_id, )
        )
        # fetchone nos devuelve el primer elemento de coincidencia
        g.user = c.fetchone()

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        
        return view(**kwargs)

    return wrapped_view
    
@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('auth.login'))