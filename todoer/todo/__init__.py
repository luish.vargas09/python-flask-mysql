# Nos permite acceder a las variables de entorno
import os
from typing import Text 
# Importamos el framework Flask
from flask import Flask

def create_app():
    app = Flask(__name__)

    app.config.from_mapping(
        # Llave de sesión
        SECRET_KEY='mykey',
        # Obtenemos de las variables de entornor los datos de conexción a la BD
        DATABASE_HOST=os.environ.get('FLASK_DATABASE_HOST'),
        DATABASE_PORT=os.environ.get('FLASK_DATABASE_PORT'),
        DATABASE_PASSWORD=os.environ.get('FLASK_DATABASE_PASSWORD'),
        DATABASE_USER=os.environ.get('FLASK_DATABASE_USER'),
        DATABASE=os.environ.get('FLASK_DATABASE')
    )
    
    from . import db
    db.init_app(app)

    from . import auth
    from . import todo

    app.register_blueprint(auth.bp)
    app.register_blueprint(todo.bp)

    @app.route('/hola')
    def hola():
        return '<h1>Chanchito Feliz</h1>'

    return app
